import dotenv from 'dotenv';
import express from 'express';
import { v4 as uuidv4 } from 'uuid';

// in memory storage for example
const messageId = uuidv4();
const queues = {
  'click-through': {
    [messageId]: {
      retries: 0,
      status: 'waiting',
      handler: 'log-click',
      data: {
        here: 'is',
        some: 'data',
      },
    },
  },
  'rate-increase': {},
};
const messages = {};

dotenv.config({ path: './.env' });

const app = express();

const queueExists = (req, res, next) => {
  if (!Object.keys(queues).includes(req.params.queue)) {
    return res.status(404).json({
      status: 'failed',
    });
  }
  next();
};

const messageExists = (req, res, next) => {
  //TODO: implement
};

app.use(express.json());

app
  .route('/api/queues/:queue/messages')
  .post(queueExists, (req, res, next) => {
    //TODO: validation, errors, etc.
    const messageId = uuidv4();
    const message = { retries: 0, status: 'waiting', ...req.body };
    queues[req.params.queue][messageId] = message;
    messages[messageId] = req.params.queue;
    return res.status(200).json({
      status: 'queued',
    });
  })
  .get(queueExists, (req, res, next) => {
    return res.status(200).json(queues[req.params.queue]);
  });

app.route('/api/messages/:messageId').patch(messageExists, (req, res, next) => {
  const queue = messages[req.params.messageId];
  const message = queues[queue][messageId];
  //TODO: acknowledge a message w/ process/failed
  // update #N retries
  // remove from queue if needed
});

app.use('/', express.static('public'));

const port = process.env.PORT;
const host = process.env.HOST;

app.listen(port, host, () => {
  console.log(`App running on ${host}:${port}`);
});
