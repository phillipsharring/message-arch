document.querySelector('#send-button').addEventListener('click', async function (event) {
  const sendToQueue = document.querySelector('#sendToQueue');
  const queue = sendToQueue.options[sendToQueue.selectedIndex].value;
  const message = document.querySelector('#message').value;
  await fetch(`/api/queues/${queue}/messages`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: message,
  });
  alert('Message queued');
});

document.querySelector('#view-button').addEventListener('click', async function (event) {
  const queueToView = document.querySelector('#queueToView');
  const queue = queueToView.options[queueToView.selectedIndex].value;
  const messages = document.querySelector('#messages');
  const res = await fetch(`/api/queues/${queue}/messages`, {
    headers: { 'Content-Type': 'application/json' },
  });
  const json = await res.json();
  messages.textContent = JSON.stringify(json, function (k, v) {
    if (v instanceof Array) return JSON.stringify(v);
    return v;
  });
});
