# Background

“The marketing intelligence platform that makes your data more meaningful.” is the catchphrase for PMG’s premier marketing platform, Alli.

In order to do this deceptively simple task, Alli has to process terabytes of client data from a plethora of sources each and every day. This data is used to gain insights and drive client marketing strategies as well as small tweaks in ad spend. In order to be most effective, multiple transformations need to be applied to the data, insights gathered, and actions generated ASAP. A further complication is there is no dependable pattern of how much time will each step in the process take.

## Problem Statement

1. In a modern modular system, different components (e.g. APIs) collaborate to perform a complex activity. These subsystems have to communicate with each other to signal the status of their portions of the work. This is further complicated with the fact that as systems evolve, new components are added or removed.

2. Generate an interface that represents a "message" between systems that will have varying concrete implementations. Your code will include a way to “listen to a message” and “return status code”.

    Most of the problem set seems to point toward implementing this solution with asynchronous messaging. The requirement for the interface to return a status code, however, is somewhat ambiguous. It's not clear if the status code is intended to indicate receipt of a given message (could we send the message?), or the result of any processing on the message (what happened to the data?).

    In a system where a given message's status indicates the result of its data's processing, that seems to imply that systems which originate messages would:

    - Keep track of messages which were sent (requiring extra storage)
    - Poll for results at later intervals, until a message is in a satisfactory status (requiring extra processing)

    Therefore, I decided to err on the side of the former; that the returned status code should indicate if the message could be sent or not. I think it is preferable that messages be truly one-way and asynchronous, to avoid the above storage & processing and to fulfill the requirement that processing time on the receiving system is unknowable.

    After whatever necessary transformation is done to the data on the receiving system, it could eventually be returned to the originating system, or sent to any number of another systems, in the form of new, separate messages. Further handling by the subsequent systems would pick up the data processing where the 1st receiving system left off.

    This one-way implementation would make message handling much easier because a given handler would only be concerned with the data it received in the message, and not be concerned with details such as the previous state of the data or keeping track of what changed.

    This also makes the 'return status' of the message sending operation into a concern of the communication protocol, and not necessarily a property of the message itself.

    Therefore, to handle the 'listening for a message,' I'm envisioning a queue that receives messages with JSON data, and the status code - the answer to 'could we send the message?' - would simply be status code of the response, which would vary depending on the implementation of the queue.

    This could be implemented as a RESTful API in numerous programming paradigms, or even be handled by a queuing system implemented with Advanced Message Queuing Protocol.

    What's interesting about the message interface then is simply:

    - What to do with the message data, that is, the handler, which could be either part of the message interface, or, in the case of a queuing system, handled by which queue a given message is put in. Different queues could be processed by different data-processing systems and handlers.
    - And, of course, the data itself, which would be a JSON payload that will vary depending on the handler.

    Metadata about the message itself, such as the time it was queued, picked-up, queuing status (processed, failed, number of retries, etc.), could be maintained by the queuing system.

    See `./message.json` for an example.

```json
{
  // optional
  "handler": "a-handler-name",
  "data": {
    "property": "value",
    "another-property: {
      "nesting": [
        "another value",
        "etc."
      ]
    }
  }
}
```

3. How will the parts of this system communicate those messages? Please produce some sort of diagram, topology overview, or text describing this.

    **Option 1:** In the case of a dedicated message queue, various systems would simply implement a client-side library for whatever queuing vendor was chosen and attempt to connect to the queue when sending a message. Worker processes on the data-processing systems would pick up messages in the various queues. (Fig. 1)

    **Option 2:** A RESTful API for receiving messages could be implemented in a stand-alone fashion. Here, again, worker processes on data-processing systems would pickup messages in the various queues, perhaps by using a GET endpoint (Fig. 2).

    **Option 3:** Finally, the API could be implemented as part of the various data-processing systems themselves, where, upon receipt of a message, they store the data locally and process it at their convenience (Fig. 3).
  
```pre
      [ originating system(s) ]
                |
          (AMQP protocol)
                |
                v
            [ queue ]
                ^
                |
      [ data processing system(s)
        queue worker processes ]
```

Fig 1: Dedicated queue system

```pre
      [ originating system(s) ]
                |
              POST
                |
                v
         [ message API ]
                ^
                |
GET, e.g. /queues/{queue}/messages?after={ts}?count={N}
                |
      [ data processing system(s)
        queue worker processes ]
```

Fig 2: Dedicated message RESTful API

```pre
      [ originating system(s) ]
                |
              POST
                |
                v
  [ data processing systems(s) RESTful API ]
```

Fig 3: RESTful endpoints integrated into each data processing system.

4. How would you go about deploying this platform in a public cloud of your choice (describe your methodology and choice of tooling in 1+ paragraphs).

    For deploying in a public cloud, I think docker containers are the easiest to implement. For **Option 1**, above, a simple docker container w/ RabbitMQ or similar system would be sufficient.

    For **Option 2**, a simple Node/Express API could easily be implemented and use a standard official docker image.

    For **Option 3**, containers built from a docker image that implements the programming paradigm of the data-processing system could be used. This could either be an official image or a custom `Dockerfile` image as needed.

5. How would you go about chunking this massive software into phases (what Minimally Viable Products would you create?). (1+ paragraphs describing at least 2 MVPs).

    In the case of **Option 2** above, I would break the project into the following pieces:

    - The RESTful API queue service itself - `(restqueue-server)` - with an endpoint to POST a new message into a given queue, as well as an endpoint to GET messages in a given queue. This is the main MVP.
    - One or more SDK-style projects that would implement a client library for communicating with this API, in any necessary programming paradigm. E.g. a PHP client service for Symfony `(restqueue-client-php)`, or a Node/Express client `(restqueue-client-node-express)`. These SDK libraries would be designed to be integrated as a standard composer or npm dependency, pulled from the SDK's git repository. As new data-processing services were brought online that needed to communicate back and forth using this queue service, they would simply implement these libraries.
  
    **Option 3** could similarly be handled in the SDK-style with libraries that also provided routing of endpoints that could easily be integrated into given systems. I would do it this way, rather than letting individual projects implement their own endpoints for receiving messages, so there weren't any inconsistencies with implementations that could lead to problems.

6. How do you divide up tasks that can be delegated to other developers on the team should they be available (1+ paragraphs describing a simple project plan)?

    I think setting up the the initial RESTful API project framework could be done by a single developer, and endpoints could be scaffolded, which could then be worked on by one or more developers. Unit tests and API documentation would be required.

    The SDK pieces for each client would be separate projects that implemented the main API's endpoints, as specified in its documentation. One or more developers could work on these together, or a different developer could implement the SDK in the language/paradigm they were most experienced with.

## Running the Demo

```bash
git clone https://gitlab.com/phillipsharring/message-arch
cd message-arch
npm install
cp .env.example .env
```

Configure the `HOST` and `PORT` in `.env` to taste.

```bash
node server.js
```

Go to <http://{HOST}:{PORT}/demo/> e.g. <http://localhost:3000/demo/>
